#ifndef REQUESTWINDOW_H
#define REQUESTWINDOW_H

#include <QWidget>
#include <QLineEdit>

class RequestWindow : public QWidget
{
    Q_OBJECT

    public:
        RequestWindow(QWidget *parent = 0);

    private:
        QLineEdit *m_t_request;
};

#endif // REQUESTWINDOW_H
