#ifndef WORDLIST_WORD_H
#define WORDLIST_WORD_H


class WordList_Word
{
public:
    WordList_Word(int p_id, int p_id_word_list, int p_id_word);
private:
    int id;
    int id_word_list;
    int id_word;
};

#endif // WORDLIST_WORD_H
