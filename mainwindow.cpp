
#include <QtWidgets>

#include "mainwindow.h"

/**
 * @brief MainWindow::MainWindow
 */
MainWindow::MainWindow()
{
    setGeometry(QApplication::desktop()->availableGeometry(0).right() - 150, QApplication::desktop()->availableGeometry(0).bottom() - 150, 150, 150);
    setWindowFlags ( Qt::Window | Qt::CustomizeWindowHint );

    QFile file(":/qss/default.qss");
    file.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(file.readAll());
    setStyleSheet(styleSheet);

    m_w_console = new ConsoleWindow(this);
    m_w_request = new RequestWindow(this);
    m_w_modules = new ModuleWindow(this);
    m_w_chatbotspeak = new ChatbotSpeakWindow(this);

    // Buttons
        m_text = new QPushButton(this);
        QPixmap* pixmap = new QPixmap(":/images/chatbot.png");
        QIcon icon(*pixmap);
        QSize iconSize(pixmap->width(), pixmap->height());
        m_text->setIconSize(iconSize);
        m_text->setIcon(icon);
        m_text->move(20,20);
        m_text->setFlat(true);

        m_modules = new QPushButton("P", this);
        m_modules->setGeometry(125, 0, 25, 25);

        m_console = new QPushButton("C", this);
        m_console->setGeometry(125, 125, 25, 25);

        m_exit = new QPushButton("Q", this);
        m_exit->setGeometry(0, 0, 25, 25);

    QObject::connect(m_modules, SIGNAL(clicked()), this, SLOT(showProgrammes()));
    QObject::connect(m_console, SIGNAL(clicked()), this, SLOT(showConsole()));
    QObject::connect(m_exit, SIGNAL(clicked()), qApp, SLOT(quit()));
    QObject::connect(m_text, SIGNAL(clicked()), this, SLOT(showSaisieText()));

    qDebug("[system] Gobtoo Started");

     // m_bouton->setFlat(true);
//    quitter = new QLabel("X", this);
//    quitter->setGeometry(130, 0, 20, 20);

//    m_bouton->setFont(QFont("Comic Sans MS", 14));
//    m_bouton->setCursor(Qt::PointingHandCursor);
//    m_bouton->move(200, 50);

//    m_lcd = new QLCDNumber(this);
//    m_lcd->setSegmentStyle(QLCDNumber::Flat);
//    m_lcd->move(50, 20);

//    m_slider = new QSlider(Qt::Horizontal, this);
//    m_slider->setGeometry(10, 60, 150, 20);
//    m_slider->setRange(200, 600);

//    m_progressbar = new QProgressBar(this);
//    m_progressbar->setGeometry(10, 80, 150, 20);
//    m_progressbar->setRange(200, 600);

//    QObject::connect(m_slider, SIGNAL(valueChanged(int)), m_lcd, SLOT(display(int))) ;
//    QObject::connect(m_slider, SIGNAL(valueChanged(int)), m_progressbar, SLOT(setValue(int))) ;


    // QObject::connect(quitter, SIGNAL(clicked()), qApp, SLOT(quit()));

//    QObject::connect(m_slider, SIGNAL(valueChanged(int)), this, SLOT(changerLargeur(int)));
//    QObject::connect(this, SIGNAL(agrandissementMax()), qApp, SLOT(quit()));

//    m_boutonDialogue = new QPushButton("Ouvrir la boîte de dialogue", this);
//    m_boutonDialogue->move(100, 120);

//    QObject::connect(m_boutonDialogue, SIGNAL(clicked()), this, SLOT(ouvrirDialogue()));

//    m_bouton2 = new QPushButton("Ouvrir la fenêtre", this);

}

/**
 * @brief MainWindow::showProgrammes
 */
void MainWindow::showProgrammes()
{
//     qDebug() << "executed";
    if (m_w_modules->isHidden())
    {
        qDebug("[info] Show modules");
        m_w_modules->setGeometry(x(), y() - 400, 150, 400);
        m_w_modules->show();
//        TODO: Change color of the m_programmes button
    }
    else
    {
        qDebug("[info] Hide modules");
        m_w_modules->hide();
//        TODO: Change color of the m_programmes button
    }

}

/**
 * @brief MainWindow::showSaisieText
 */
void MainWindow::showSaisieText()
{
    if (m_w_request->isHidden())
    {
        if (m_w_console->isVisible())
        {
            qDebug("[info] Hide console");
            m_w_console->hide();
//        TODO: Change color of the m_w_console button
        }
        qDebug("[info] Show request text input");
        m_w_request->setGeometry(x() - 175, y() + 70, 200, 25);
        m_w_request->show();
    }
    else
    {
        qDebug("[info] Hide request text input");
        m_w_request->hide();
    }
}

/**
 * @brief MainWindow::showConsole
 */
void MainWindow::showConsole()
{
    if (m_w_console->isHidden())
    {
        if (m_w_request->isVisible())
        {
            qDebug("[info] Hide request text input");
            m_w_request->hide();
        }
        qDebug("[info] Show console");
        m_w_console->setGeometry(x() - 400, y(), 400, 150);
        m_w_console->show();
//        TODO: Change color of the m_w_console button
    }
    else
    {
        qDebug("[info] Hide console");
        m_w_console->hide();
//        TODO: Change color of the m_w_console button
    }
}

/**
 * @brief MainWindow::showChatbotSpeak
 * @param chatbot_text
 */
void MainWindow::showChatbotSpeak(QString chatbot_text)
{
    m_w_chatbotspeak->setGeometry(x()- 400, y() - 150, 400, 150);
    m_w_chatbotspeak->new_speak(chatbot_text);
    m_w_chatbotspeak->show();
}

/**
 * @brief MainWindow::request_sent_line_edit
 * Process the request enter in a line_edit
 * @param line_edit
 */
void MainWindow::request_sent_line_edit(QLineEdit *line_edit)
{
    if ( ! line_edit->text().isEmpty())
    {
        qDebug("[user] " + line_edit->text().toLatin1());
        processing_request(line_edit->text());
        line_edit->setText("");
    }
    else
    {
        qDebug("[warning] line_edit empty");
    }

}

/**
 * @brief MainWindow::processing_request
 * Process the request pass in parameter
 * @param request
 * TODO: Organise the request for each module and find a solution to add easily new request
 */
void MainWindow::processing_request(QString request)
{
    // Write the request in the log and the console
    showChatbotSpeak(request);

    // Analyse the request

}
