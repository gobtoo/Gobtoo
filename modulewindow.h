#ifndef MODULEWINDOW_H
#define MODULEWINDOW_H


#include <QApplication>
#include <QWidget>
#include <QPushButton>
#include <QLCDNumber>
#include <QSlider>
#include <QPixmap>
#include <QBitmap>
#include <QLabel>
#include <QMessageBox>
#include <QInputDialog>
#include <QVBoxLayout>
#include <QDebug>

#include "chatbotsettingswindow.h"

class ModuleWindow : public QWidget
{
    Q_OBJECT

    public:
        ModuleWindow(QWidget *parent = 0);

    public slots:
        void showGobtoo();

    private:
        ChatbotSettingsWindow *m_w_chatbot_settings;
        QPushButton *m_chatbot_settings;
        QPushButton *m_main_settings;
        QPushButton *m_exit;
};

#endif // MODULEWINDOW_H
