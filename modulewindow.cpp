#include "modulewindow.h"

/**
 * @brief ModuleWindow::ModuleWindow
 * @param parent
 */
ModuleWindow::ModuleWindow(QWidget *parent)
    : QWidget(parent)
{
    setWindowFlags ( Qt::Dialog | Qt::CustomizeWindowHint );

    m_w_chatbot_settings = new ChatbotSettingsWindow();

    m_chatbot_settings = new QPushButton("Chatbot", this);
    m_main_settings = new QPushButton("Main Settings", this);
    m_exit = new QPushButton("Exit", this);

    m_chatbot_settings->setGeometry(0, 300, 150, 25);
    m_main_settings->setGeometry(0, 350, 150, 25);
    m_exit->setGeometry(0, 375, 150, 25);

    QObject::connect(m_chatbot_settings, SIGNAL(clicked()), this, SLOT(showGobtoo()));
    QObject::connect(m_exit, SIGNAL(clicked()), qApp, SLOT(quit()));
}

/**
 * @brief ModuleWindow::showGobtoo
 */
void ModuleWindow::showGobtoo()
{
    if (m_w_chatbot_settings->isHidden())
    {
        qDebug("[info] Show Chatbot Settings");
//        m_w_gobtoo->setGeometry(x(), y(), 500, 500);
        m_w_chatbot_settings->showMaximized();
    }
    else
    {
        qDebug("[info] Hide Chatbot Settings");
        m_w_chatbot_settings->hide();
    }

}
