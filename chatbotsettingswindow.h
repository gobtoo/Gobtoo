#ifndef CHATBOTSETTINGSWINDOW_H
#define CHATBOTSETTINGSWINDOW_H

#include <QApplication>
#include <QLineEdit>
#include <QTextEdit>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QGridLayout>
#include <QTabWidget>
#include <QFormLayout>
#include <QDebug>

class ChatbotSettingsWindow : public QWidget
{
    Q_OBJECT

    public:
        ChatbotSettingsWindow(QWidget *parent = 0);

    public slots:
        void submit_question_answer();

    private:
        QTabWidget *m_tab;
        QWidget *m_add_question_answer;
        QLabel *m_l_question;
        QLineEdit *m_t_question;
        QLabel *m_l_answer;
        QTextEdit *m_t_answer;
        QPushButton *m_submit;
        QPushButton *m_exit;
        QHBoxLayout *m_button_layout;
        QGridLayout *m_layout;
        QVBoxLayout *m_layout_chatbot_settings;
};

#endif // CHATBOTSETTINGSWINDOW_H
