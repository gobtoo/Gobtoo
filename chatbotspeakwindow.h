#ifndef CHATBOTSPEAKWINDOW_H
#define CHATBOTSPEAKWINDOW_H

#include <QDebug>
#include <QWidget>
#include <QTextEdit>
#include <QLabel>
#include <QTabWidget>
#include <QTabBar>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>

class ChatbotSpeakWindow : public QWidget
{
    Q_OBJECT

    public:
        ChatbotSpeakWindow(QWidget *parent = 0);

    public slots:
        void new_speak(QString chatbot_text);
        void close_current_tab();
        void close_all_tabs();

    private:
        QTabWidget *onglets;
        QTextEdit *m_te_speak;
        QPushButton *m_vote_up;
        QLabel *m_vote_count;
        QPushButton *m_vote_down;
        QPushButton *m_b_close_all_tabs;
        QPushButton *m_b_propose_answer;
        QVBoxLayout *m_layout_vote;
        QHBoxLayout *m_layout_onglets_vote;
        QHBoxLayout *m_layout_buttons;
        QVBoxLayout *m_layout;
};

#endif // CHATBOTSPEAKWINDOW_H
