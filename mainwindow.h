#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>

#include "modulewindow.h"
#include "consolewindow.h"
#include "requestwindow.h"
#include "chatbotspeakwindow.h"

class MainWindow : public QWidget
{
    Q_OBJECT

    public:
        MainWindow();

    public slots:
        void showProgrammes();
        void showSaisieText();
        void showConsole();
        void showChatbotSpeak(QString chatbot_text);
        void processing_request(QString request);
        void request_sent_line_edit(QLineEdit *line_edit);
        void request_sent() {
            request_sent_line_edit(dynamic_cast<QLineEdit*>(sender()));
        }


    private:
        QPushButton *m_modules;
        QPushButton *m_console;
        QPushButton *m_exit;
        QPushButton *m_text;
        ModuleWindow *m_w_modules;
        ConsoleWindow *m_w_console;
        RequestWindow *m_w_request;
        ChatbotSpeakWindow *m_w_chatbotspeak;
        QLCDNumber *m_lcd;
        QSlider *m_slider;
        QLabel *imageLabel;
        QLabel *quitter;
        QProgressBar *m_progressbar;
};

#endif // MAINWINDOW_H
