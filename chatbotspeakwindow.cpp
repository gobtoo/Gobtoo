#include "chatbotspeakwindow.h"

/**
 * @brief ChatbotSpeakWindow::ChatbotSpeakWindow
 * @param parent
 */
ChatbotSpeakWindow::ChatbotSpeakWindow(QWidget *parent)
    : QWidget(parent)
{
    setWindowFlags ( Qt::Dialog | Qt::CustomizeWindowHint );

    onglets = new QTabWidget(this);

    onglets->setTabsClosable(true);
    // TODO: Not always display vote (only if it's an anwer to a question)
    m_vote_up = new QPushButton(this);
    QPixmap* pixmap = new QPixmap(":/images/vote_up.png");
    QIcon icon(*pixmap);
    QSize iconSize(pixmap->width(), pixmap->height());
    m_vote_up->setIconSize(iconSize);
    m_vote_up->setIcon(icon);
    m_vote_up->move(20,20);
    m_vote_up->setFlat(true);

    m_vote_count = new QLabel(this);
    m_vote_count->setText("9"); // TODO: Center this text and get it from the database


    m_vote_down = new QPushButton(this);
    QPixmap* pixmap2 = new QPixmap(":/images/vote_down.png");
    QIcon icon2(*pixmap2);
    QSize iconSize2(pixmap2->width(), pixmap2->height());
    m_vote_down->setIconSize(iconSize2);
    m_vote_down->setIcon(icon2);
    m_vote_down->move(20,20);
    m_vote_down->setFlat(true);

    m_b_close_all_tabs = new QPushButton("Close all tabs");
    m_b_propose_answer = new QPushButton("I want to propose another answer");

    m_layout_vote = new QVBoxLayout();
    m_layout_vote->addWidget(m_vote_up);
    m_layout_vote->addWidget(m_vote_count);
    m_layout_vote->addWidget(m_vote_down);

    m_layout_onglets_vote = new QHBoxLayout();
    m_layout_onglets_vote->addWidget(onglets);
    m_layout_onglets_vote->addLayout(m_layout_vote);

    m_layout_buttons = new QHBoxLayout();
    m_layout_buttons->addWidget(m_b_close_all_tabs);
    m_layout_buttons->addWidget(m_b_propose_answer);

    m_layout = new QVBoxLayout(this);
    m_layout->addLayout(m_layout_onglets_vote);
    m_layout->addLayout(m_layout_buttons);
    m_layout->setMargin(1);


    QObject::connect(onglets, SIGNAL(tabCloseRequested(int)), this, SLOT(close_current_tab()));
    QObject::connect(m_b_close_all_tabs, SIGNAL(clicked()), this, SLOT(close_all_tabs()));

}

/**
 * @brief ChatbotSpeakWindow::new_speak
 * @param chatbot_text
 */
void ChatbotSpeakWindow::new_speak(QString chatbot_text)
{
    qDebug("[chatbot] " + chatbot_text.toLatin1());
    m_te_speak = new QTextEdit(this);
    m_te_speak->setReadOnly(true);
    m_te_speak->setText(chatbot_text);
    onglets->addTab(m_te_speak, "a"); // TODO: Set the good name for the tab

    QTabBar *tabBar = onglets->findChild<QTabBar *>();
    if (onglets->count() == 1)
    {
        tabBar->hide();
    }
    else
    {
        tabBar->show();
    }
}

/**
 * @brief ChatbotSpeakWindow::close_current_tab
 */
void ChatbotSpeakWindow::close_current_tab()
{
    qDebug("[info] Close tab");
    onglets->removeTab(onglets->currentIndex());
    if (onglets->count() <= 0)
    {
        hide();
    }
    else
    {
        QTabBar *tabBar = onglets->findChild<QTabBar *>();
        if (onglets->count() == 1)
        {
            tabBar->hide();
        }
        else
        {
            tabBar->show();
        }
    }
}

/**
 * @brief ChatbotSpeakWindow::close_all_tabs
 */
void ChatbotSpeakWindow::close_all_tabs()
{
    qDebug("[info] Close all tab");
    onglets->clear();
    hide();
}
