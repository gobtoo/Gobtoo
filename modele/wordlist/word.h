#ifndef WORD_H
#define WORD_H


class Word
{
public:
    Word(int p_id, QString p_word);
private:
    int id;
    QString word;
};

#endif // WORD_H
