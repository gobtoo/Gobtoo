#ifndef LANGUAGE_H
#define LANGUAGE_H


class Language
{
    public:
        Language(int pId, QString pLabel);

    private:
        int id;
        QString label;
};

#endif // LANGUAGE_H
