# Gobtoo :fish:
Hello and Welcome on this Wiki.

This is a start for a very big project promoting open source ! :thumbsup:

## Goal !
The purpose of this project is to help the user with a chatbot :smiley_cat: (you can call this artificial intelligence but he will not be able to evolve himself, it's based on user participation (so if you are afraid about robot that take control of your data and kill the word :boom: don't worry this is only composed of a simple algorithm that search the best answer based on key words)).

The chatbot will be connected to an internet server and users will be invited to rate answer and questions, propose new questions, new answer (and the database will grow).

Then the project will include open source projects as "module" (like music player, mailclient, office pack, antivirus, browser, social network, chat...), the chatbot will be able to give support on this modules and auto update every module.

Then develop mobile app on the same idea. :iphone:

The goal is also to get together devs around main open source projects (today there is a lot of project but something is often missing to one another (this is my point of view)).

I also want to provide encrypted data exchange and anonymous connexion.

It's a lot of work and i will need to rebuild form scratch a lot of things but it's my dream and i think we really can bring open source to a lot of people and improve the open source world with this idea.

If you want to join this project feel free to contact me.

> "The man who moves a mountain begins by carrying away small stones."  
> **Confucius**

Have a nice day. :sunny: 

## TODO v0.0.5:
* [x] Create a good log system (can be accessed easily, write logs in files, different lvl of logs)
* [ ] Choose and create Database
* [ ] Automatize Database filling for genesis brain
* [ ] Implement the algorithm for the chatbot
* [ ] Fr Translation :fr:

## Informations
This software is build with **Qt 5.4.1**
