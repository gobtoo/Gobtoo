#include "chatbotsettingswindow.h"

/**
 * @brief ChatbotSettingsWindow::ChatbotSettingsWindow
 * @param parent
 */
ChatbotSettingsWindow::ChatbotSettingsWindow(QWidget *parent)
    : QWidget(parent)
{
//    resize(400, 500);

//    m_gobtoo->setGeometry(0, 300, 150, 25);
    m_layout_chatbot_settings = new QVBoxLayout;
    m_tab = new QTabWidget(this);
    m_add_question_answer = new QWidget(this);

    m_l_question = new QLabel("Question");
    m_t_question = new QLineEdit();

    m_l_answer = new QLabel("Answer");
    m_t_answer = new QTextEdit();

    m_submit = new QPushButton("Submit");
    m_exit = new QPushButton("Close");



    m_button_layout = new QHBoxLayout();
    m_button_layout->addWidget(m_submit);
    m_button_layout->addWidget(m_exit);

    m_layout = new QGridLayout;
    m_layout->addWidget(m_l_question, 0, 0);
    m_layout->addWidget(m_t_question, 0, 1);
    m_layout->addWidget(m_l_answer, 1, 0, 1, 2);
    m_layout->addWidget(m_t_answer, 2, 0, 1, 2);
    m_layout->addLayout(m_button_layout, 3, 0, 1, 2);

    m_add_question_answer->setLayout(m_layout);

    m_tab->addTab(m_add_question_answer, "Add a question");

    m_layout_chatbot_settings->addWidget(m_tab);

    this->setLayout(m_layout_chatbot_settings);

    QObject::connect(m_submit, SIGNAL(clicked()), this, SLOT(submit_question_answer()));
    QObject::connect(m_exit, SIGNAL(clicked()), this, SLOT(close()));

//    TODO: Allow user to choose to display vote options
}

/**
 * @brief ChatbotSettingsWindow::submit_question_answer
 */
void ChatbotSettingsWindow::submit_question_answer()
{
    qDebug("submit_question_answer executed");
//    TODO: create the function and add the answer to the database
}

