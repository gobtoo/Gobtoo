#-------------------------------------------------
#
# Project created by QtCreator 2015-04-10T18:25:02
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Gobtoo
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    requestwindow.cpp \
    consolewindow.cpp \
    modulewindow.cpp \
    chatbotspeakwindow.cpp \
    chatbotsettingswindow.cpp \
    modele/language.cpp \
    modele/wordlist/wordlist.cpp \
    modele/wordlist/word.cpp \
    modele/wordlist/wordlist_word.cpp

HEADERS  += mainwindow.h \
    requestwindow.h \
    consolewindow.h \
    modulewindow.h \
    chatbotspeakwindow.h \
    chatbotsettingswindow.h \
    modele/language.h \
    modele/wordlist/wordlist.h \
    modele/wordlist/word.h \
    modele/wordlist/wordlist_word.h

FORMS    +=

RESOURCES += \
    application.qrc

DISTFILES += \
    qss/default.qss
