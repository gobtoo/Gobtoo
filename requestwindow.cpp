#include "requestwindow.h"

/**
 * @brief RequestWindow::RequestWindow
 * @param parent
 */
RequestWindow::RequestWindow(QWidget *parent)
    : QWidget(parent)
{
    setWindowFlags ( Qt::Dialog | Qt::CustomizeWindowHint );
    m_t_request = new QLineEdit(this);
    m_t_request->setGeometry(0,0, 200, 25);

    QObject::connect(m_t_request, SIGNAL(returnPressed()), parent, SLOT(request_sent()));
}
