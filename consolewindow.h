#ifndef CONSOLEWINDOW_H
#define CONSOLEWINDOW_H

#include <QWidget>
#include <QPushButton>
#include <QLCDNumber>
#include <QSlider>
#include <QPixmap>
#include <QBitmap>
#include <QLabel>
#include <QProgressBar>
#include <QMessageBox>
#include <QInputDialog>
#include <QVBoxLayout>
#include <QDebug>
#include <QTextEdit>
#include <QLineEdit>
#include <QTextStream>
#include <QFile>
#include <QDir>
#include <QFileSystemWatcher>
#include <QByteArray>

class ConsoleWindow : public QWidget
{
    Q_OBJECT

    public:
        ConsoleWindow(QWidget *parent = 0);

    public slots:
        void update_console();
        void add_text_console(QString text_console, bool line_break = true);

    private:
        QWidget *m_parent;
        QVBoxLayout *m_layout;
        QLineEdit *m_t_request;
        QTextEdit *m_l_console;
        QString const_log_file_name;
        QFileSystemWatcher log_watcher;
        int log_file_nb_line_read;
};

#endif // CONSOLEWINDOW_H
