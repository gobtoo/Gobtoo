#include <QApplication>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QFileInfo>
#include "mainwindow.h"

void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QByteArray localMsg = msg.toLocal8Bit();
    QString txt;
    switch (type) {
    case QtDebugMsg:
        fprintf(stderr, "[DEBUG   ] %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        txt = QString("[DEBUG   ] %1").arg(localMsg.constData());
        break;
    case QtWarningMsg:
        fprintf(stderr, "[WARNING ] %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        txt = QString("[WARNING ] %1 (%2:%3, %4)").arg(localMsg.constData()).arg(context.file).arg(context.line).arg(context.function);
        break;
    case QtCriticalMsg:
        fprintf(stderr, "[CRITICAL] %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        txt = QString("[CRITICAL] %1 (%2:%3, %4)").arg(localMsg.constData()).arg(context.file).arg(context.line).arg(context.function);
        break;
    case QtFatalMsg:
        fprintf(stderr, "[FATAL   ] %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        txt = QString("[FATAL   ] %1 (%2:%3, %4)").arg(localMsg.constData()).arg(context.file).arg(context.line).arg(context.function);
    }
    QFile outFile(QDir::currentPath() + "/application.log");
    outFile.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream ts(&outFile);
    ts << txt << endl;
    if (type == QtFatalMsg)
    {
        abort();
    }
}

void prepare_logs_files() {
    if (QFileInfo::exists(QDir::currentPath() + "/application.log")) {
        if (QFileInfo::exists(QDir::currentPath() + "/last_use_application.log")) {
            QFile::remove(QDir::currentPath() + "/last_use_application.log");
        }
        QFile::rename(QDir::currentPath() + "/application.log", QDir::currentPath() + "/last_use_application.log");
    }
}

/**
 * @brief main
 * @param argc
 * @param argv
 * @return
 * TODO: Add an update module who try to git pull and recompile code
 */
int main(int argc, char *argv[])
{
    prepare_logs_files();
    qInstallMessageHandler(myMessageOutput);
    qDebug("[system] Logs are ready");
    QApplication app(argc, argv);

    // Création de la fenêtre
    MainWindow window;

    window.show();

    return app.exec();

}
