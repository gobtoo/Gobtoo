#ifndef WORDLIST_H
#define WORDLIST_H


class WordList
{
    public:
        WordList(int p_id, int p_id_language, QString p_name);

    private:
        int id;
        int id_language;
        QString name;
};

#endif // WORDLIST_H
