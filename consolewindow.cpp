﻿#include "consolewindow.h"

/**
 * @brief ConsoleWindow::ConsoleWindow
 * @param parent
 */
ConsoleWindow::ConsoleWindow(QWidget *parent)
    : QWidget(parent)
{
    const_log_file_name = "application.log";
    log_file_nb_line_read = 0;

    setWindowFlags ( Qt::Dialog | Qt::CustomizeWindowHint );

    m_l_console = new QTextEdit(this);
    m_l_console->setReadOnly(true);

    m_t_request = new QLineEdit();

    m_layout = new QVBoxLayout(this);
    m_layout->addWidget(m_l_console);
    m_layout->addWidget(m_t_request);

    this->setLayout(m_layout);

    log_watcher.addPath(QDir::currentPath() + "/" + const_log_file_name);

    QObject::connect(m_t_request, SIGNAL(returnPressed()), parent, SLOT(request_sent()));
    QObject::connect(&log_watcher, SIGNAL(fileChanged(QString)), this, SLOT(update_console()));

}

/**
 * @brief ConsoleWindow::update_console
 */
void ConsoleWindow::update_console()
{
    QFile log_file(QDir::currentPath() + "/" + const_log_file_name);
    if (log_file.exists()) {
        if (log_file.open(QIODevice::ReadOnly | QIODevice::Text)){
            for (int i = 0; i < log_file_nb_line_read; i++)
            {
                log_file.readLine();
            }
            QByteArray line;
            while (! log_file.atEnd())
            {
                line = log_file.readLine();
                add_text_console(line, log_file_nb_line_read > 0); // For the first line no line break before
                log_file_nb_line_read++;

            }
        }
        log_file.close();
    }
    m_l_console->moveCursor(QTextCursor::End);
}

/**
 * @brief ConsoleWindow::add_text_console
 * This function allow you to add text into the console windows
 * @param text_console
 * @param jump_line
 * @pre text_console != Null
 */
void ConsoleWindow::add_text_console(QString text_console, bool line_break)
{
    m_l_console->moveCursor(QTextCursor::End);
    if (text_console.startsWith("[DEBUG   ] [user]") or text_console.startsWith("[DEBUG   ] [chatbot]"))
    {
        text_console = "<b>" + text_console + "</b>";
    }
    else if (text_console.startsWith("[DEBUG   ] [warning]"))
    {
        text_console = "<b><font color=\"orange\">" + text_console + "</font></b>";
    }
    else if (text_console.startsWith("[DEBUG   ] [error]"))
    {
        text_console = "<b><font color=\"red\">" + text_console + "</font></b>";
    }

    if (line_break)
    {
        m_l_console->textCursor().insertHtml("<br>" + text_console);
    }
    else
    {
        m_l_console->textCursor().insertHtml(text_console);
    }

    m_l_console->moveCursor(QTextCursor::End);
}

